package com.centeroflife.petregistrationapi.exception;

import com.centeroflife.petregistrationapi.constant.APIConstant;
import com.centeroflife.petregistrationapi.model.response.APIBaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleAllExceptions(Exception ex, WebRequest request) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(APIConstant.STATUS_FAIL, ex.getMessage(),
				HttpStatus.INTERNAL_SERVER_ERROR.toString());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

//	@ExceptionHandler(BadRequestException.class)
//	public final ResponseEntity<ExceptionResponse> handleBadRequestExceptions(Exception ex, WebRequest request) {
//		ExceptionResponse exceptionResponse = new ExceptionResponse(APIConstant.STATUS_FAIL, ex.getMessage(),
//				HttpStatus.BAD_REQUEST.toString());
//		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
//	}

	@ExceptionHandler(BusinessException.class)
	public final ResponseEntity<APIBaseResponse> handleBusinessException(BusinessException ex, WebRequest request) {
		APIBaseResponse baseResponse = new APIBaseResponse();
		baseResponse.setResponseStatus(APIConstant.STATUS_FAIL);
		baseResponse.setErrorCode(ex.getErrorConfig().getErrorCode());
		baseResponse.setMessage(ex.getErrorConfig().getErrorMessage());

		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}

}