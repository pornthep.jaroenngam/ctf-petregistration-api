package com.centeroflife.petregistrationapi.service.registration;

import com.centeroflife.petregistrationapi.dao.PetRegistrationDao;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class PetRegistrationServiceImpl implements PetRegistrationService{
	@Resource
	PetRegistrationDao petRegistrationDao;

	@Override
	public List<PetRegistrationData> findAll() {
		return petRegistrationDao.findAll();
	}

	@Override
	public void insertPetRegistration(PetRegistrationData data) {
		petRegistrationDao.insertPetRegistration(data);
	}

	@Override
	public void updatePetRegistration(PetRegistrationData data) {

	}

	@Override
	public void executeUpdatePetRegistration(PetRegistrationData data) {

	}

	@Override
	public void deletePetRegistration(PetRegistrationData data) {

	}

	@Override
	public boolean isExistData(PetRegistrationData data) {
		return petRegistrationDao.isExistData(data);
	}
}
