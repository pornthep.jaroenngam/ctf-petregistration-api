package com.centeroflife.petregistrationapi.constant;

public class CommonConstant {

	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAIL = "FAIL";
	public static final String COMMA = ",";
	public static final String SPACE = " ";
}
