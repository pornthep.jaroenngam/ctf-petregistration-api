package com.centeroflife.petregistrationapi.dao;

import com.centeroflife.petregistrationapi.constant.CommonConstant;
import com.centeroflife.petregistrationapi.constant.DatabaseConstant.PetOwnerField;
import com.centeroflife.petregistrationapi.model.entity.PetOwner;
import com.centeroflife.petregistrationapi.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PetOwnerDaoImpl implements PetOwnerDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate template;

    public PetOwnerDaoImpl(NamedParameterJdbcTemplate template) {
        this.template = template;
    }


    @Override
    public List<PetOwner> findAll() {
        return null;
    }

    @Override
    public void insertPetOwner(PetOwner data) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("insert into pet_owner")
                .append("(").append(PetOwnerField.OWNER_CID).append(CommonConstant.COMMA)
                .append(PetOwnerField.TITLE_NAME).append(CommonConstant.COMMA)
                .append(PetOwnerField.FIRSTNAME).append(CommonConstant.COMMA)
                .append(PetOwnerField.LASTNAME).append(CommonConstant.COMMA)
                .append(PetOwnerField.GENDER).append(CommonConstant.COMMA)
                .append(PetOwnerField.BIRTHDATE).append(CommonConstant.COMMA)
                .append(PetOwnerField.ADDRESS_NO).append(CommonConstant.COMMA)
                .append(PetOwnerField.BUILDING).append(CommonConstant.COMMA)
                .append(PetOwnerField.SOI).append(CommonConstant.COMMA)
                .append(PetOwnerField.ROAD).append(CommonConstant.COMMA)
                .append(PetOwnerField.SUBDISTRICT).append(CommonConstant.COMMA)
                .append(PetOwnerField.DISTRICT).append(CommonConstant.COMMA)
                .append(PetOwnerField.PROVINCE).append(CommonConstant.COMMA)
                .append(PetOwnerField.ZIP_CODE).append(CommonConstant.COMMA)
                .append(PetOwnerField.OWNER_TYPE).append(CommonConstant.COMMA)
                .append(PetOwnerField.ORGANIZATION).append(CommonConstant.COMMA)
                .append(PetOwnerField.TELEPHONE_NO).append(CommonConstant.COMMA)
                .append(PetOwnerField.MOBILE_NO).append(")")
                .append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        System.out.println("SQL : "+ sqlBuilder.toString());

        jdbcTemplate.update(sqlBuilder.toString(),
                data.getOwnerCid(),
                data.getTitleName(),
                data.getFirstName(),
                data.getLastName(),
                data.getGender(),
                DateUtils.getDateFromString(data.getBirthDate()),
                data.getAddressNo(),
                data.getBuilding(),
                data.getSoi(),
                data.getRoad(),
                data.getSubdistrict(),
                data.getDistrict(),
                data.getProvince(),
                data.getZipCode(),
                data.getOwnerType(),
                data.getOrganization(),
                data.getTelephoneNo(),
                data.getMobileNo());
    }

    @Override
    public void updatePetOwner(PetOwner data) {

    }

    @Override
    public void executeUpdatePetOwner(PetOwner data) {

    }

    @Override
    public void deletePetOwner(PetOwner data) {

    }

    @Override
    public boolean isExistData(String ownerCid) {
        String sql = "SELECT count(*) FROM pet_owner WHERE owner_cid = ?";
        boolean result = false;
        int count = jdbcTemplate.queryForObject(sql, new Object[]{ownerCid}, Integer.class);
        if (count > 0) {
            result = true;
        }
        return result;
    }
}
