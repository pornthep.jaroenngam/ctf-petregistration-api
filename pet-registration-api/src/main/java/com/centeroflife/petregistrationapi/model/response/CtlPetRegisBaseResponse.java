package com.centeroflife.petregistrationapi.model.response;

public class CtlPetRegisBaseResponse {
	protected String responseStatus;
	protected String message;
	protected String errorCode;

	public CtlPetRegisBaseResponse() {
	}

	public CtlPetRegisBaseResponse(String responseStatus, String message) {
		this.responseStatus = responseStatus;
		this.message = message;
		this.errorCode = "";
	}


	public CtlPetRegisBaseResponse(String responseStatus, String message, String errorCode) {
		this.responseStatus = responseStatus;
		this.message = message;
		this.errorCode = errorCode;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String errorMessage) {
		this.message = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
