package com.centeroflife.petregistrationapi.constant;

public class MessageConstant {
	public static final String SIGN_UP_SUCCESSFUL = "การลงทะเบียนสำเร็จ";
	public static final String CHANGE_PIN_SUCCESSFUL = "เปลี่ยนรหัสผ่านสำเร็จ";
	public static final String UPDATE_FCM_TOKEN_SUCCESSFUL = "update fcm token successful";
	public static final String VERIFY_RESET_PASSWORD_SUCCESSFUL = "การยืนยันตัวตนเพื่อเปลี่ยนรหัสผ่านสำเร็จ";
	public static final String RESET_PASSWORD_SUCCESSFUL = "เปลี่ยนรหัสผ่านสำเร็จ";

	public static final String MICROCHIP_NO_ALREADY_EXISTS = "รหัสไม่โครชิพนี้ได้ลงทะเบียนแล้ว";
	public static final String OWNER_REGISTER_SUCCESSFUL = "ลงทะเบียนสำเร็จ";
}
