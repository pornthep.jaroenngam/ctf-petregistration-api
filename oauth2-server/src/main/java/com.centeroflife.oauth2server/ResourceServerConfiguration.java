package com.centeroflife.oauth2server;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "ctf-resource-id";
    private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
    private static final String SECURED_PATTERN = "/api/**";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
//        http.requestMatchers()
//                .antMatchers(SECURED_PATTERN).and().authorizeRequests()
//                .antMatchers(HttpMethod.POST, SECURED_PATTERN).access(SECURED_WRITE_SCOPE)
//                .anyRequest().access(SECURED_READ_SCOPE);

//        http.
//                anonymous().disable()
//                .requestMatchers().antMatchers(SECURED_PATTERN)
//                .and().authorizeRequests()
//                .antMatchers(SECURED_PATTERN).access("hasRole('ADMIN') or hasRole('USER')")
//                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
//
//        http.authorizeRequests().antMatchers("/admin/**")
//                .access("hasRole('ADMIN')")
//                .and().formLogin().loginPage("/login")
//                .and().exceptionHandling().accessDeniedPage("/403");

        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                //.antMatchers("/css/**","/img/**","/login/**", "/oauth/**").permitAll()
                .antMatchers("/oauth/token").permitAll()
                                .anyRequest().authenticated()
                                .and()
                                .formLogin().loginPage("/login").and().exceptionHandling().accessDeniedPage("/403");


        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests().antMatchers("/oauth/token").permitAll().
                anyRequest().authenticated();
    }

}
