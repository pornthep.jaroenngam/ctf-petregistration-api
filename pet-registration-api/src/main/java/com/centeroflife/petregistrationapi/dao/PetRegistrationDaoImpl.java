package com.centeroflife.petregistrationapi.dao;

import com.centeroflife.petregistrationapi.constant.CommonConstant;
import com.centeroflife.petregistrationapi.constant.DatabaseConstant.PetRegistrationDataField;
import com.centeroflife.petregistrationapi.constant.ErrorConstant;
import com.centeroflife.petregistrationapi.exception.BusinessException;
import com.centeroflife.petregistrationapi.model.entity.PetOwner;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;
import com.centeroflife.petregistrationapi.mapper.PetRegistrationRowMapper;
import com.centeroflife.petregistrationapi.utils.CommonUtils;
import com.centeroflife.petregistrationapi.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class PetRegistrationDaoImpl implements PetRegistrationDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate template;

	@Resource
	PetOwnerDao petOwnerDao;

    public PetRegistrationDaoImpl(NamedParameterJdbcTemplate template) {
        this.template = template;
    }


    @Override
    public List<PetRegistrationData> findAll() {
        return template.query("select * from pet_registration_data", new PetRegistrationRowMapper());
    }

    @Override
    public void insertPetRegistration(PetRegistrationData data) {

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("insert into pet_registration_data")
                .append("(").append(PetRegistrationDataField.MICROCHIP_NO).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.PET_NAME).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.PET_BIRTH_DATE).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.PET_SEX).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.PET_TYPE).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.PET_SPECIES).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.OWNER_CID).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.MICROCHIP_INJECT_DATE).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.MICROCHIP_INJECT_PLACE).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.VETERINARY_OWNER).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.REGIS_PROVINCE).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.REGIS_DISTRICT).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.REGIS_SUBDISTRICT).append(CommonConstant.COMMA)
                .append(PetRegistrationDataField.PET_STATUS).append(")")
                .append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        System.out.println("SQL : "+ sqlBuilder.toString());


		if(!petOwnerDao.isExistData(data.getOwnerCid())){
			throw new BusinessException(ErrorConstant.ErrorConfig.PET_OWNER_NO_EXISTS);
		}

        jdbcTemplate.update(sqlBuilder.toString(),
				data.getMicrochipNo(),
				data.getPetName(),
				DateUtils.getDateFromString(data.getPetBirthDate()),
				data.getPetSex(),
				data.getPetType(),
				data.getPetSpecies(),
				data.getOwnerCid(),
				DateUtils.getDateFromString(data.getMicrochipInjectDate()),
				data.getMicrochipInjectPlace(),
				data.getVeterinaryOwner(),
				data.getRegisProvince(),
				data.getRegisDistrict(),
				data.getRegisSubdistrict(),
				data.getPetStatus());
    }

    @Override
    public void updatePetRegistration(PetRegistrationData emp) {

    }

    @Override
    public void executeUpdatePetRegistration(PetRegistrationData emp) {

    }

    @Override
    public void deletePetRegistration(PetRegistrationData emp) {

    }

	@Override
	public boolean isExistData(PetRegistrationData data) {
		String sql = "SELECT count(*) FROM pet_registration_data WHERE microchip_no = ?";
		boolean result = false;
		int count = jdbcTemplate.queryForObject(sql, new Object[] { data.getMicrochipNo() }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}
}
