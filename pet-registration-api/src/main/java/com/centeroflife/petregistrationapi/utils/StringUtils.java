package com.centeroflife.petregistrationapi.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class StringUtils {
    public static boolean isEmptyString(String str) {
        return str == null || "".equals(str);
    }

    public static String verifyNull(String str) {
        return str == null ? "" : str;
    }
    
    public static String verifyNullAndTrim(String str) {
        return str == null ? "" : str.trim();
    }
    
    
    public static String verifyEmptyAndDat(String str) {
        return str == null || str.trim().equals("-") ? "" : str.trim();
    }

    public static String removeEnd(String str) {
        return str.substring(0, str.length() - 1);
    }


    public static String removeSpace(String str){
        return str.replace(" ","");
    }

    public static String urlEncoder(String str){
        try {
            str = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return str;
    }

}
