package com.centeroflife.petregistrationapi.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "pet_owner")
@Getter
@Setter
public class PetOwner {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "owner_id")
//    private int ownerId;

    @Id
    @Column(name = "owner_cid")
    private String ownerCid;

    @Column(name = "title_name")
    private String titleName;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "birth_date")
    private String birthDate;

    @Column(name = "address_no")
    private String addressNo;

    @Column(name = "building")
    private String building;

    @Column(name = "soi")
    private String soi;

    @Column(name = "road")
    private String road;

    @Column(name = "subdistrict")
    private String subdistrict;

    @Column(name = "district")
    private String district;

    @Column(name = "province")
    private String province;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "owner_type")
    private String ownerType;

    @Column(name = "organization")
    private String organization;

    @Column(name = "mobile_no")
    private String mobileNo;

    @Column(name = "telephone_no")
    private String telephoneNo;

    public PetOwner(String ownerCid) {
        this.ownerCid = ownerCid;
    }
}
