package com.centeroflife.petregistrationapi.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pet_registration_data")
@Getter
@Setter
public class PetRegistrationData {

    @Id
    @Column(name = "microchip_no")
    private String microchipNo;

    @Column(name = "pet_name")
    private String petName;

    @Column(name = "pet_birth_date")
    private String petBirthDate;

    @Column(name = "pet_sex")
    private String petSex;

    @Column(name = "pet_type")
    private String petType;

    @Column(name = "pet_species")
    private String petSpecies;

    @Column(name = "owner_cid")
    private String ownerCid;

    @Column(name = "microchip_inject_date")
    private String microchipInjectDate;

    @Column(name = "microchip_inject_place")
    private String microchipInjectPlace;

    @Column(name = "veterinary_owner")
    private String veterinaryOwner;

    @Column(name = "regis_province")
    private String regisProvince;

    @Column(name = "regis_district")
    private String regisDistrict;

    @Column(name = "regis_subdistrict")
    private String regisSubdistrict;

    @Column(name = "pet_status")
    private String petStatus;
}
