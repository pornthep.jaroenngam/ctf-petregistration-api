package com.centeroflife.petregistrationapi.exception;

public class ExceptionResponse {
  
	protected String responseStatus;
	protected String errorMessage;
	protected String errorCode;
		
	public ExceptionResponse(String responseStatus, String errorMessage, String errorCode) {
		super();
		this.responseStatus = responseStatus;
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}