package com.centeroflife.petregistrationapi.constant;

import java.util.HashMap;
import java.util.Map;

public class ErrorConstant {

	public enum ErrorConfig {
		MICROCHIP_NO_EXISTS("RG_00001", ErrorConstant.ERROR_MSG_USER_NOT_EXIST),
		PET_OWNER_NO_EXISTS("PO_00001", ErrorConstant.ERROR_MSG_PET_OWNER_NOT_EXIST),
		PET_OWNER_IS_EXISTS("PO_00002", ErrorConstant.ERROR_MSG_PET_OWNER_IS_EXIST);


		
		private final String errorCode;
		private final String errorMessage;

		private static final Map<String, String> MAP = new HashMap<String, String>();
		static {
			for (ErrorConfig s : ErrorConfig.values()) {
				MAP.put(s.errorCode, s.errorMessage);

			}
		}
		private ErrorConfig(String errorCode, String errorMessage) {
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}
		public String getErrorCode() {
			return errorCode;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
		
		
	}

	public static final String ERROR_MSG_PET_OWNER_NOT_EXIST = "ไม่พบข้อมูลเจ้าของในระบบ";
	public static final String ERROR_MSG_PET_OWNER_IS_EXIST = "ตรวจพบว่า หมายเลขบัตรประชาชนนี้ได้ทำการลงทะเบียนแล้ว";

	public static final String ERROR_MSG_DATA_NOT_FOUND = "ไม่พบข้อมูลในระบบ";
	public static final String ERROR_MSG_CARD_ID_EMPTY = "ไม่มีเลขบัตรประจำตัวในระบบ";
	public static final String ERROR_MSG_CARD_ID_IS_USED = "หมายเลขบัตรประจำตัวนี้ได้ทำการลงทะเบียนแล้ว";
	public static final String ERROR_MSG_CURRENT_PIN_WRONG = "รหัสผ่านเดิมไม่ถูกต้อง";
	public static final String ERROR_MSG_CHANGE_PIN_NOT_AVALIABLE = "ไม่สามารถเปลี่ยนรหัสผ่านได้";
	public static final String ERROR_MSG_INITIAL_DATA_USER_NOT_FOUND = "ไม่พบผู้ใช้ในระบบ";
	public static final String ERROR_MSG_TICKET_NOT_FOUND = "ไม่พบตั๋วจำนำในระบบ";
	public static final String ERROR_MSG_NOT_ACTIVATE = "บัญชีผู้ใช้ของท่านยังไม่สามารถใช้งานได้ กรุณาติดต่อเจ้าหน้าที่เพื่อเปิดใช้งานบัญชี";
	public static final String ERROR_MSG_USER_NOT_EXIST = "ไม่พบข้อมูลผู้ใช้ในระบบ กรุณาติดต่อเจ้าหน้าที่";
	public static final String ERROR_MSG_VERIFY_RESET_DATEOFBIRTH_NOT_MATCH = "วันเกิดที่คุณกรอกไม่ตรงกับข้อมูลที่มีในระบบ";
	public static final String ERROR_MSG_VERIFY_RESET_MOBILENO_NOT_MATCH = "เบอร์โทรศัพท์คุณกรอกไม่ตรงกับข้อมูลที่มีในระบบ";
	public static final String ERROR_MSG_VERIFY_RESET_SECURE_MSG_NOT_MATCH = "ข้อความกันลืมรหัสผ่านไม่ตรงกับข้อมูลที่มีในระบบ";
	public static final String ERROR_MSG_WRONG_PASSWORD = "ขออภัย รหัสผ่านไม่ถูกต้อง";
}
