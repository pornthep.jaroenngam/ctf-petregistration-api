package com.centeroflife.petregistrationapi.constant;

public class DatabaseConstant {
   public static class PetRegistrationDataField{
        public static final String MICROCHIP_NO = "microchip_no";
        public static final String PET_NAME = "pet_name";
        public static final String PET_BIRTH_DATE = "pet_birth_date";
        public static final String PET_SEX = "pet_sex";
        public static final String PET_TYPE = "pet_type";
        public static final String PET_SPECIES = "pet_species";
        public static final String OWNER_CID = "owner_cid";
        public static final String MICROCHIP_INJECT_DATE = "microchip_inject_date";
        public static final String MICROCHIP_INJECT_PLACE = "microchip_inject_place";
        public static final String VETERINARY_OWNER = "veterinary_owner";
        public static final String REGIS_PROVINCE = "regis_province";
        public static final String REGIS_DISTRICT = "regis_district";
        public static final String REGIS_SUBDISTRICT = "regis_subdistrict";
        public static final String PET_STATUS = "pet_status";
    }

     public static class PetOwnerField{
          public static final String OWNER_CID = "owner_cid";
          public static final String TITLE_NAME = "title_name";
          public static final String FIRSTNAME = "firstname";
          public static final String LASTNAME = "lastname";
          public static final String GENDER = "gender";
          public static final String BIRTHDATE = "birth_date";
          public static final String ADDRESS_NO = "address_no";
          public static final String BUILDING = "building";
          public static final String SOI = "soi";
          public static final String ROAD = "road";
          public static final String SUBDISTRICT = "subdistrict";
          public static final String DISTRICT = "district";
          public static final String PROVINCE = "province";
          public static final String ZIP_CODE = "zip_code";
          public static final String OWNER_TYPE = "owner_type";
          public static final String ORGANIZATION = "organization";
          public static final String TELEPHONE_NO = "telephone_no";
          public static final String MOBILE_NO = "mobile_no";

     }
}
