package com.centeroflife.petregistrationapi.service.petowner;


import com.centeroflife.petregistrationapi.model.entity.PetOwner;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;

import java.util.List;

public interface PetOwnerService {
	List<PetOwner> findAll();

	void insertPetOwner(PetOwner data);

	void updatePetOwner(PetOwner data);

	void executeUpdatePetOwner(PetOwner data);

	void deletePetOwner(PetOwner data);

	boolean isExistData(String ownerCid);
	
}
