package com.centeroflife.petregistrationapi.dao;

import com.centeroflife.petregistrationapi.model.entity.PetOwner;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;

import java.util.List;

;

public interface PetOwnerDao {

	List<PetOwner> findAll();

	void insertPetOwner(PetOwner data);

	void updatePetOwner(PetOwner data);

	void executeUpdatePetOwner(PetOwner data);

	void deletePetOwner(PetOwner data);

	boolean isExistData(String ownerCid);
}
