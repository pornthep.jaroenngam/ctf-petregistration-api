package com.centeroflife.petregistrationapi.utils;

import java.nio.charset.Charset;
import java.util.Base64;

public class Base64Utils {
    public static String encode(String token) {
        byte[] encodedBytes = Base64.getEncoder().encode(token.getBytes());
        return new String(encodedBytes, Charset.forName("UTF-8"));
    }


    public static String decode(String token) {
        byte[] decodedBytes = Base64.getDecoder().decode(token.getBytes());
        return new String(decodedBytes, Charset.forName("UTF-8"));
    }
}
