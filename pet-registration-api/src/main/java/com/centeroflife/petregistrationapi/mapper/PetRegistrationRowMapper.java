package com.centeroflife.petregistrationapi.mapper;

import com.centeroflife.petregistrationapi.constant.DatabaseConstant.*;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PetRegistrationRowMapper implements RowMapper<PetRegistrationData> {


	@Override
	public PetRegistrationData mapRow(ResultSet rs, int i) throws SQLException {
		PetRegistrationData data = new PetRegistrationData();
		data.setMicrochipNo(rs.getString(PetRegistrationDataField.MICROCHIP_NO));
		data.setPetName(rs.getString(PetRegistrationDataField.PET_NAME));
		data.setPetBirthDate(rs.getString(PetRegistrationDataField.PET_BIRTH_DATE));
		data.setPetSex(rs.getString(PetRegistrationDataField.PET_SEX));
		data.setPetType(rs.getString(PetRegistrationDataField.PET_TYPE));
		data.setPetSpecies(rs.getString(PetRegistrationDataField.PET_SPECIES));
		data.setOwnerCid(rs.getString(PetRegistrationDataField.OWNER_CID));
		data.setMicrochipInjectDate(rs.getString(PetRegistrationDataField.MICROCHIP_INJECT_DATE));
		data.setMicrochipInjectPlace(rs.getString(PetRegistrationDataField.MICROCHIP_INJECT_PLACE));
		data.setVeterinaryOwner(rs.getString(PetRegistrationDataField.VETERINARY_OWNER));
		data.setRegisProvince(rs.getString(PetRegistrationDataField.REGIS_PROVINCE));
		data.setRegisDistrict(rs.getString(PetRegistrationDataField.REGIS_DISTRICT));
		data.setRegisSubdistrict(rs.getString(PetRegistrationDataField.REGIS_SUBDISTRICT));
		data.setPetStatus(rs.getString(PetRegistrationDataField.PET_STATUS));

		return data;
	}

}
