package com.centeroflife.petregistrationapi.constant;

public class APIConstant {

	public static final String OAUTH_TOKEN_URL = "http://localhost:8898/pawnshop/oauth/token";
	public static final String OAUTH_AUTHORIZE_URL = "http://localhost:8898/pawnshop/oauth/authorize";
	public static final String OAUTH_CALLBACK_URL = "http://localhost:8898/pawnshop/callback";

	public static final String FIREBASE_PUSH_URL = "https://fcm.googleapis.com/fcm/send";
	public static final String FIREBASE_AUTHORIZATION = "key=AAAAGs7eC20:APA91bFUpoBONkRuSfV15CxYTNrgPH2k7NvCEtX_oVfnI2kkGcz4i1qXXhFXdxAgoYM96bvgTDW7O8rLs2KTiVPvVZLoZacbWd2nDDfCalTY7Vj_g6Lu5EvaYwex6112tJFl00oa_XEr";

	public static final String OAUTH_CLIENT_ID = "pawnshop-client";
	public static final String OAUTH_SECRET = "$2a$10$36gruYoa6HVoqp990gV3h.T2dBPrn6J2xp.mZSKj2BXaKBp30nbYm";

	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAIL = "FAIL";
	public static final String COMMA = ",";
	public static final String SPACE = " ";


	public static final String NOTIFICATION_TITLE = "แจ้งเตือนตั๋วจำนำหมดอายุ";
	//public static final String NOTIFICATION_MESSAGE = "ตั๋วจำนำของคุณ $name เลขที่ $pawnshopNo ครบกำหนดต่อดอกเบี้ยภายในวันที่ $sendDate ขออภัยหากชำระแล้ว";
	public static final String NOTIFICATION_MESSAGE = "ตั๋วจำนำ เลขที่ $pawnshopNo จะหมดอายุ ในวันที่ $sendDate";

	public static final String INTEREST_OVER = "ของหลุดจำนำ";

	public static final String PRODUCT_FILTER_NEW = "NEW";
	public static final String PRODUCT_FILTER_PMO = "PMO";
	public static final String PRODUCT_FILTER_REC = "REC";
	public static final String PRODUCT_FILTER_ALL = "ALL";

	public static final String[] REDEEM_TYPE_ALLOW = {"0"};
	public static final String[] FG_HAND_ALLOW = {"0"};
}
