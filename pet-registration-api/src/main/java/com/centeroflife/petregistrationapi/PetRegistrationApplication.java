package com.centeroflife.petregistrationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableResourceServer
public class PetRegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetRegistrationApplication.class, args);
	}
}

