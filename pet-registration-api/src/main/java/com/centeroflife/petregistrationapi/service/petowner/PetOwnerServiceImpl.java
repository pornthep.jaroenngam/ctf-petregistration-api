package com.centeroflife.petregistrationapi.service.petowner;

import com.centeroflife.petregistrationapi.dao.PetOwnerDao;
import com.centeroflife.petregistrationapi.dao.PetRegistrationDao;
import com.centeroflife.petregistrationapi.model.entity.PetOwner;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;
import com.centeroflife.petregistrationapi.service.registration.PetRegistrationService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class PetOwnerServiceImpl implements PetOwnerService {
	@Resource
	PetOwnerDao petOwnerDao;

	@Override
	public List<PetOwner> findAll() {
		return null;
	}

	@Override
	public void insertPetOwner(PetOwner data) {
		petOwnerDao.insertPetOwner(data);
	}

	@Override
	public void updatePetOwner(PetOwner data) {

	}

	@Override
	public void executeUpdatePetOwner(PetOwner data) {

	}

	@Override
	public void deletePetOwner(PetOwner data) {

	}

	@Override
	public boolean isExistData(String ownerCid) {
		return petOwnerDao.isExistData(ownerCid);
	}
}
