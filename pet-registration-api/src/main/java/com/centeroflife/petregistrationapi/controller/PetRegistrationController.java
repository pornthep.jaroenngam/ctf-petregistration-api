package com.centeroflife.petregistrationapi.controller;

import com.centeroflife.petregistrationapi.constant.CommonConstant;
import com.centeroflife.petregistrationapi.constant.MessageConstant;
import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;
import com.centeroflife.petregistrationapi.model.response.CtlPetRegisBaseResponse;
import com.centeroflife.petregistrationapi.service.registration.PetRegistrationService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static com.centeroflife.petregistrationapi.constant.ErrorConstant.ErrorConfig.*;

@RestController
//@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class PetRegistrationController {

    @Resource
    PetRegistrationService petRegistrationService;

    @PostMapping("createPetRegistration")
    public CtlPetRegisBaseResponse createPetRegistration(@RequestBody PetRegistrationData request) {

        if (!petRegistrationService.isExistData(request)) {

            petRegistrationService.insertPetRegistration(request);

            CtlPetRegisBaseResponse response = new CtlPetRegisBaseResponse();
            response.setResponseStatus(CommonConstant.STATUS_SUCCESS);
            response.setErrorCode("");
            response.setMessage(MessageConstant.SIGN_UP_SUCCESSFUL);
            return response;
        } else {
            CtlPetRegisBaseResponse response = new CtlPetRegisBaseResponse();
            response.setResponseStatus(CommonConstant.STATUS_FAIL);
            response.setErrorCode(MICROCHIP_NO_EXISTS.getErrorCode());
            response.setMessage(MICROCHIP_NO_EXISTS.getErrorMessage());
            return response;
        }
    }

}
