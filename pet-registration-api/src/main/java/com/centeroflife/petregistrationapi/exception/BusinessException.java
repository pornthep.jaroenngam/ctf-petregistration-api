package com.centeroflife.petregistrationapi.exception;

import com.centeroflife.petregistrationapi.constant.ErrorConstant;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.OK)
public class BusinessException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 8786132554918628226L;

	private ErrorConstant.ErrorConfig errorConfig;

	public BusinessException(ErrorConstant.ErrorConfig errorConfig) {
		super(errorConfig.getErrorMessage());
		this.errorConfig = errorConfig;
	}

	public ErrorConstant.ErrorConfig getErrorConfig() {
		return errorConfig;
	}

	public void setErrorConfig(ErrorConstant.ErrorConfig errorConfig) {
		this.errorConfig = errorConfig;
	}
	
	

}
