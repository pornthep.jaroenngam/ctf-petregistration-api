package com.centeroflife.petregistrationapi.service.registration;


import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;

import java.util.List;

public interface PetRegistrationService {
	List<PetRegistrationData> findAll();

	void insertPetRegistration(PetRegistrationData data);

	void updatePetRegistration(PetRegistrationData data);

	void executeUpdatePetRegistration(PetRegistrationData data);

	void deletePetRegistration(PetRegistrationData data);

	boolean isExistData(PetRegistrationData data);
	
}
