package com.centeroflife.petregistrationapi.dao;

import com.centeroflife.petregistrationapi.model.entity.PetRegistrationData;;

import java.util.List;

public interface PetRegistrationDao {

	List<PetRegistrationData> findAll();

	void insertPetRegistration(PetRegistrationData emp);

	void updatePetRegistration(PetRegistrationData emp);

	void executeUpdatePetRegistration(PetRegistrationData emp);

	void deletePetRegistration(PetRegistrationData emp);

	boolean isExistData(PetRegistrationData data);
}
