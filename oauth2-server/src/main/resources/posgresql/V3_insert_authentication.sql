
INSERT INTO
  oauth_client_details (
    client_id,
    client_secret,
    resource_ids,
    scope,
    authorized_grant_types,
    access_token_validity,
    refresh_token_validity
  )
VALUES
  (
    'appclient',
    '$2a$08$dKyJRiySVD6wP.TElU52kOdkBq.8qId0o8KjwJqIEE5PjkwZQUoWu',
    'carInventory',
    'read,write',
    'authorization_code,check_token,refresh_token,password',
    25000,
    500000
  );
--ctf-client:ctf-secret-1234
--Header Authorization : Basic Y3RmLWNsaWVudDpjdGYtc2VjcmV0LTEyMzQ=

INSERT INTO OAUTH_CLIENT_DETAILS (CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
		VALUES('ctf-client', 'ctf-resource-id',
			/*ctf-secret-1234*/
			'$2a$08$pIOTsAse7upmWxqJZZRbmOmcmKr9GYYyJIUi74N7vbFJfIixGftx.', 'read,write', 'authorization_code,check_token,refresh_token,password', 'USER', 25000, 500000);


INSERT INTO
  oauth_client_details (
    client_id,
    client_secret,
    resource_ids,
    scope,
    authorized_grant_types,
    access_token_validity,
    refresh_token_validity
  )
VALUES
  (
    'ctfclient',
    '$2a$08$MXBudUe5Nj6tcV1yQBC.XOzMl4Z.6LvU91I1NHds/L5XSrJmVL.ZK',
    'ctfresource',
    'read,write',
    'authorization_code,check_token,refresh_token,password',
    25000,
    500000
  );