package com.centeroflife.petregistrationapi.controller;

import com.centeroflife.petregistrationapi.constant.CommonConstant;
import com.centeroflife.petregistrationapi.constant.MessageConstant;
import com.centeroflife.petregistrationapi.model.entity.PetOwner;
import com.centeroflife.petregistrationapi.model.response.CtlPetRegisBaseResponse;
import com.centeroflife.petregistrationapi.service.petowner.PetOwnerService;
import com.centeroflife.petregistrationapi.service.registration.PetRegistrationService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static com.centeroflife.petregistrationapi.constant.ErrorConstant.ErrorConfig.MICROCHIP_NO_EXISTS;
import static com.centeroflife.petregistrationapi.constant.ErrorConstant.ErrorConfig.PET_OWNER_IS_EXISTS;

@RestController
//@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class PetOwnerController {

    @Resource
    PetOwnerService petOwnerService;

    @PostMapping("createPetOwner")
    public CtlPetRegisBaseResponse createPetOwner(@RequestBody PetOwner request) {

        if (!petOwnerService.isExistData(request.getOwnerCid())) {

            petOwnerService.insertPetOwner(request);

            CtlPetRegisBaseResponse response = new CtlPetRegisBaseResponse();
            response.setResponseStatus(CommonConstant.STATUS_SUCCESS);
            response.setErrorCode("");
            response.setMessage(MessageConstant.OWNER_REGISTER_SUCCESSFUL);
            return response;
        } else {
            CtlPetRegisBaseResponse response = new CtlPetRegisBaseResponse();
            response.setResponseStatus(CommonConstant.STATUS_FAIL);
            response.setErrorCode(PET_OWNER_IS_EXISTS.getErrorCode());
            response.setMessage(PET_OWNER_IS_EXISTS.getErrorMessage());
            return response;
        }
    }

}
