package com.centeroflife.petregistrationapi.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {


    public static Date getDateFromString(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            return cal.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static String getDate(String date) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            calendar.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e2) {
            e2.printStackTrace();
            return "";
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
        return sdf2.format(calendar.getTime());
    }

    public static String getTime(String date) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
        try {
            calendar.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e2) {
            e2.printStackTrace();
            return "";
        }

        return sdf2.format(calendar.getTime());
    }

//    public static boolean isUpdateDate(String oldDate, String newDate) {
//        if (!StringUtils.isEmptyString(oldDate) && !StringUtils.isEmptyString(newDate)) {
//            Calendar calOldDate = convertStringToCal(oldDate);
//            Calendar calNewDate = convertStringToCal(newDate);
//            return calNewDate.after(calOldDate);
//        }else{
//            return true;
//        }
//    }
//
//    public static String getStringDate(Calendar cal) {
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        return sdf.format(cal.getTime());
//    }
//
//
//    public static String getStringTime(Calendar cal) {
//        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
//        return sdf.format(cal.getTime());
//    }
//
//    public static Calendar convertStringToCal(String date) {
//        Calendar calendar = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//
//        try {
//            calendar.setTime(sdf.parse(date));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        } catch (NullPointerException e2) {
//            e2.printStackTrace();
//            return calendar;
//        }
//        return calendar;
//    }


}
